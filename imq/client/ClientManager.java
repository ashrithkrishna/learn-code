package imq.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Date;

import imq.database.DatabaseManager;
import imq.dto.Client;

public class ClientManager extends Thread
{
    final DataInputStream dis; 
    final DataOutputStream dos; 
    final Socket socket; 

    public ClientManager(Socket socket, DataInputStream dis, DataOutputStream dos)  
    { 
        this.socket = socket; 
        this.dis = dis; 
        this.dos = dos; 
    } 
  
    @Override
    public void run()  
    { 
    	Date date = new Date();
    	String connectedTime = new Timestamp(date.getTime()).toString();
    	
        String received; 
        String toreturn; 
        
        DatabaseManager databaseManager=new DatabaseManager();
        Client client= new Client();
        
        while (true)  
        { 
            try 
            { 
                // Ask user what he wants 
                dos.writeUTF("Tell me Mr.Client on port number "+socket.getPort()+" Inet : "+socket.getInetAddress()); 
                  
                // receive the answer from client 
                received = dis.readUTF();
                
                prepareClient(client,socket,received,connectedTime);
                
                databaseManager.addtoDB(client);
                
                //Echo Server
                toreturn = received;
                System.out.println("Client on "+socket.getPort()+" says "+received);
                
                dos.writeUTF(toreturn); 
                
                if(received.equals("Exit") || received.equals("Bye")) 
                {  
                	closeConnection(this.socket);
                	break;
                } 
            } 
            catch (IOException e) 
            { 
            	System.out.println("Client on "+socket.getPort()+" disconnected abruptly");
            	prepareClient(client,socket,"Unexpected Termination",connectedTime);
            	databaseManager.addtoDB(client);
                break;
            } 
        } 
          
        try 
        {
			this.dis.close();
			this.dos.close();
		} 
        catch (IOException e) 
        {
			e.printStackTrace();
		} 
    }

	private void closeConnection(Socket socket) throws IOException 
	{
        System.out.println("\nClient " + this.socket + " wants to close connection"); 
        System.out.println("Closing this connection."); 
        try 
        {
			socket.close();
			System.out.println("Connection closed!\n");
		} 
        catch (IOException e) 
        {
			e.printStackTrace();
		} 
	}

	private void prepareClient(Client client, Socket socket, String received, String connectedTime) 
	{
		client.setPortNumber(socket.getPort());
        client.setClientIP(socket.getInetAddress().toString().substring(1));
        client.setData(received);
        client.setConnectedTime(connectedTime);
	}
}
