import java.io.*; 
import java.net.*;
import java.util.Scanner; 

public class Client 
{ 
	public static void main(String[] args) throws IOException 
	{ 
		try
		{ 
			Scanner scanner = new Scanner(System.in); 
			
			// getting localhost ip 
			InetAddress ip = InetAddress.getByName("localhost"); 
	
			// establish the connection with server port 5056 
			Socket socket = new Socket(ip, 5056); 
	
			// obtaining input and out streams 
			DataInputStream dis = new DataInputStream(socket.getInputStream()); 
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream()); 
	
			// the following loop performs the exchange of information between client and clientManager
			while (true) 
			{ 
				System.out.println(dis.readUTF()); 
				String tosend = scanner.nextLine(); 
				dos.writeUTF(tosend); 
				
				String received = dis.readUTF(); 
				System.out.println("Server says : "+received); 
				
				if(tosend.equals("Exit") || tosend.equals("Bye")) 
				{ 
					closeConnection(socket);
					break; 
				} 
			} 
			
			scanner.close(); 
			dis.close(); 
			dos.close(); 
		}
		catch(Exception e)
		{ 
			e.printStackTrace(); 
		} 
	}

	private static void closeConnection(Socket socket)
	{
		System.out.println("Closing this connection : " + socket); 
		try 
		{
			socket.close();
			System.out.println("Connection closed"); 
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
	} 
} 
