package imq.database;

import java.sql.*;
import java.util.Date;

import imq.dto.Client;

public class DatabaseManager 
{
	Connection connection;
	Statement statement;
	
	public DatabaseManager()
	{
		try 
		{
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/imq-ashrith","root","root");  
			statement=connection.createStatement();  
			//ResultSet rs=stmt.executeQuery("select * from emp");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}    
	}
	
	public void addtoDB(Client client)
	{
		Date date = new Date();
		PreparedStatement preparedStatement;
		
		try 
		{
			preparedStatement = connection.prepareStatement("insert into root(portNumber,clientIP,data,datasendTimeStamp,connectedTime) values(?,?,?,?,?)");
			preparedStatement.setInt(1, client.getPortNumber());
			preparedStatement.setString(2, client.getClientIP());
			preparedStatement.setString(3, client.getData());
			preparedStatement.setString(4, new Timestamp(date.getTime()).toString());
			preparedStatement.setString(5, client.getConnectedTime());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}  
	}
}
