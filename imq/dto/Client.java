package imq.dto;

public class Client 
{
	private int id;
	private int portNumber;
	private String clientIP;
	private String data;
	private String datasendTimeStamp;
	private String connectedTime;
	
	public String getConnectedTime() {
		return connectedTime;
	}
	public void setConnectedTime(String connectedTime) {
		this.connectedTime = connectedTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPortNumber() {
		return portNumber;
	}
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDatasendTimeStamp() {
		return datasendTimeStamp;
	}
	public void setDatasendTimeStamp(String datasendTimeStamp) {
		this.datasendTimeStamp = datasendTimeStamp;
	}
}
