package imq.server;

import java.net.*;

import imq.client.ClientManager;

import java.io.*; 

public class Server 
{ 
	public static void main(String[] args) throws IOException  
    { 
        @SuppressWarnings("resource")
		ServerSocket serverSocket = new ServerSocket(5056); 

        System.out.println("Server Started. Listening on port 5056.");
        
        while (true)  
        { 
            Socket socket = null; 	// socket object to receive incoming client requests 
            try 
            { 
                socket = serverSocket.accept(); 
                  
                System.out.println("A new client is connected : " + socket); 
                  
                DataInputStream dis = new DataInputStream(socket.getInputStream()); 
                DataOutputStream dos = new DataOutputStream(socket.getOutputStream()); 
                
                // create a new thread object 
                Thread thread = new ClientManager(socket, dis, dos); 
                thread.start();   
            } 
            catch (Exception exception)
            { 
            	socket.close(); 
            	exception.printStackTrace(); 
            } 
        } 
    } 
} 
