package com.itt.LearnGrow;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Tumblr 
{
	public static void main(String args[])
	{
		getBlogInfo();
	}
	
	public static void getBlogInfo()
	{
		java.util.Scanner input=new java.util.Scanner(System.in);
		System.out.println("Enter the tumblr blog name : ");
		String blogName = input.next();
		System.out.println("Enter the Range : ");
		String range = input.next();
		
		ClientResponse response = getResponse(range);
		
		String output = response.getEntity(String.class);
		output=output.substring(22,output.length()-2);
		
		printBlogInfo(output);
	}

	private static void printBlogInfo(String output)
	{
		try 
		{
			JSONObject jsonObject1 = (JSONObject) new JSONParser().parse(output);
			String postsTotal=jsonObject1.get("posts-total").toString();
			
			JSONObject jsonObject2 = (JSONObject) new JSONParser().parse(jsonObject1.get("tumblelog").toString());
			String title=jsonObject2.get("title").toString();
			String description=jsonObject2.get("description").toString();
			String name=jsonObject2.get("name").toString();
			
			System.out.println("Number of posts : "+postsTotal+"\nTitle : "+title+"\nDescription : "+description+"\nName : "+name);
			
			printPhotoURLs(jsonObject1);
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}	
	}

	private static void printPhotoURLs(JSONObject jsonObject1) 
	{
		JSONArray postsArray=(JSONArray) jsonObject1.get("posts");
		Iterator<JSONObject> iterator = postsArray.iterator();
		while(iterator.hasNext())
		{
			System.out.println(iterator.next().get("photo-url-1280"));
		}
	}

	private static ClientResponse getResponse(String range) 
	{
		Client client = Client.create();
		WebResource webResource = client.resource("https://good.tumblr.com/api/read/json?type=photo");
		
		ClientResponse response = webResource
				.queryParam("num", range.substring(range.indexOf("-")+1))
				.queryParam("start", range.substring(0,range.indexOf("-")))
				.accept("application/json")
				.header("Content-Type","application/json")
				.get(ClientResponse.class);
		return response;
	}
}
