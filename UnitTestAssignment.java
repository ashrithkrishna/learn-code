import java.util.*;

public class UnitTestAssignmentSelf 
{
    public static void main(String args[]) 
    {
        int testCasesArray[];

        testCasesArray=getInputFromUser();

        for(int testcase : testCasesArray)
        {
            System.out.println(mainLogic(testcase));
        }
    }

    static int[] getInputFromUser()
    {
        while(true)
        {
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter number of test cases:");

            int numberOfTestCases=scan.nextInt();
            int testCasesArray[] = new int[numberOfTestCases];

            if(numberOfTestCases>0)
            {
                for (int i=0;i<numberOfTestCases;i++)
                {
                    int userInput=scan.nextInt();
                    if(userInput > 0)
                    {
                        testCasesArray[i]=userInput;
                    }
                    else
                    {
                        System.out.println("Please enter a positive number");
                        i--;
                    }
                }
            }
            else
            {
                System.out.println("Enter a valid number!");
            }

            scan.close();
            return testCasesArray;
        }
    }

    static int getNumberOfDivisors(int number)
    {
        int count = 0;
        for (int k =1; k<=Math.sqrt(number); k++)
        {
            if(number%k==0)
            {
                if(number/k==k)
                count++;

                count =count+2;
            }
        }
        return count;
    }

    static int mainLogic(int n)
    {
        int pairCount=0;

        for(int i=1;i<n;i++)
        {
            int numberofDivisorsOffirstDigit=getNumberOfDivisors(i);
            int numberofDivisorsOfSecondDigit=getNumberOfDivisors(i+1);

            if(numberofDivisorsOffirstDigit==numberofDivisorsOfSecondDigit)
            {
                pairCount++;
            }

            else
            {
                continue;
            }
        }
        return pairCount;
    }
}