class Assignment5
{
    public static void main(String[] args) 
    {
        PaperBoy paperBoy = new PaperBoy();
        paperBoy.requestForPay();
    }
}

class Customer 
{
    private String firstName; 
    private String lastName; 
    private Wallet myWallet = new Wallet();

    public String getFirstName()
    {
        return firstName; 
    } 

    public String getLastName()
    { 
        return lastName; 
    } 

    public float getPayment(float billAmount) 
    {        
        if (myWallet != null) 
        {            
            if (myWallet.getTotalMoney() > billAmount)
            {                
                myWallet.subtractMoney(billAmount);                
                return billAmount;            
            }        
        }   
        return 0; 
    }
} 

class Wallet 
{
    private static float value; 

    public float getTotalMoney() 
    { 
        return value; 
    } 
    public void setTotalMoney(float newValue) 
    { 
        value = newValue; 
    } 
    public void addMoney(float deposit) 
    { 
        value += deposit; 
    } 
    public void subtractMoney(float debit) 
    { 
        value -= debit; 
    } 
} 

class PaperBoy
{
    public void requestForPay()
    {
        float payment = 2.00f;
        Customer myCustomer = new Customer();

        float paidAmount = myCustomer.getPayment(payment);

        if (paidAmount == payment) 
        {        
            System.out.println("Payment of Rs "+paidAmount+" received.Thank you.");
        } 
        else 
        {         
            System.out.println("I will come back for payment");
        }
    }
}