import random

def validateNumber(userGuess):
    if userGuess.isdigit() and 1<= int(userGuess) <=100:
        return True
    else:
        return False

def main():
    randomNumber=random.randint(1,100)
    flag=False
    userGuess=input("Guess a number between 1 and 100:")
    numberofAttempts=0

    while not flag:
        if not validateNumber(userGuess):
            userGuess=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            numberofAttempts+=1
            userGuess=int(userGuess)

        if userGuess<randomNumber:
            userGuess=input("Too low. Guess again")

        elif userGuess>randomNumber:
            userGuess=input("Too High. Guess again")

        else:
            print('You guessed it in '+str(numberofAttempts)+' guesses!')
            flag=True

main()
