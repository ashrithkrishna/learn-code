import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

class AnimalFeed
{
	public static void main(String args[])
	{
		Animal animal=new Animal();
		
		while(true)
		{
			animal.hungryAnimal();
		}
	}
}

class Animal
{
	public void hungryAnimal()
	{
		Feeder feeder=new Feeder();
		Scanner scanner=new Scanner(System.in);
		
		ThreadLocalRandom random = ThreadLocalRandom. current(); 
	    int randomNumber = random. nextInt(1, 11);
	    
	    try
	    { 
	        Thread.sleep(randomNumber*1000);
	        System.out.println("I am hungry\nFeed: Y? N?");
	        String userInput=scanner.next();
	        
	        if(userInput.equalsIgnoreCase("y"))
	        {
				float food=feeder.getFood(10);
				System.out.println("Obtained food : "+food);

				if(food != 0)
				{
					feeder.feedFood(food);
				}
				else
				{
					System.out.println("Animal died!");
	        		System.exit(0);
				}
	        }
	        else
	        {
	        	System.out.println("Animal died!");
	        	System.exit(0);
	        }
	    }
	    catch(InterruptedException e)
	    {
			System.out.println("An exception occured. Please restart the program. ");
			scanner.close();
	    }
	}
}

class Feeder
{
	FoodStock foodStock=new FoodStock();
	
	public float getFood(float requiredAmountOfFood) 
	{
		if (foodStock != null) 
		{
			if (foodStock.getTotalFoodStock() >= requiredAmountOfFood) 
			{                
				foodStock.subtractFoodStock(requiredAmountOfFood);                
				return requiredAmountOfFood;            
			}
			else
			{
				System.out.println("Food out of stock. Remaining : "+foodStock.getTotalFoodStock());
				return 0;
			}
		}  
		return 0;
	}
	public void feedFood(float feedFood)
	{
		System.out.println("Animal is fed!");
	}
}
class FoodStock 
{
	private static float foodStock;
	
	public float getTotalFoodStock() 
	{        
		return foodStock;    
	}
	public void addFoodStock(float deposit)
	{
		foodStock += deposit;    
	}
	public void subtractFoodStock(float debit) 
	{
		foodStock = foodStock - debit;
	}
}