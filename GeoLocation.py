from opencage.geocoder import OpenCageGeocode
from ipyleaflet import *

key = '1781d260aee14ee5b3e32c28dde30218'
geocoder = OpenCageGeocode(key)

query = input("Enter the address of the location : ")

results = geocoder.geocode(query)

print("\nLatitude :"+str(results[0]['geometry']['lat'])+"\nLongitude : "+str(results[0]['geometry']['lng']))

userInput = input("Do you want to open this location in map? Y or N ?\n")

if(userInput == "Y"):
    results = geocoder.geocode(query)
    center = (results[0]['geometry']['lat'], results[0]['geometry']['lng'])

    map = Map(center=center, zoom=6)
    marker = Marker(location=center, draggable=False)
    map.add_layer(marker)

    map