import random;

def generateRandomNumber(upperLimit):
    randomNumber=random.randint(1, upperLimit)
    return randomNumber


def main():
    maximumNumber=6
    flag=True
    while flag:
        userChoice=input("Ready to roll? Enter Q to Quit")
        if userChoice.lower() !="q":
            diceNumber=generateRandomNumber(maximumNumber)
            print("You have rolled a "+diceNumber)
        else:
            flag=False

main()
