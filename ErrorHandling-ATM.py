import sqlite3
from urllib.request import pathname2url

atm_balance=10000

def get_card_status(card):
    conn = sqlite3.connect('Bank.db')

    cursor = conn.execute("SELECT CARD_NUMBER,CARD_STATUS from Account_Details;")
    card_status="Invalid"
    for row in cursor:
        if (row[0]==card):
            card_status=row[1]

    return card_status

    conn.close()    

def verify_pin(card_number):
    
    try:
        conn = sqlite3.connect('Bank.db')
    except sqlite3.OperationalError:
        print("Connection to the bank server was unsuccessful. Please try again later.")

    while(True):
        try:
            user_pin=int(input('Enter you pin: \n'))
            break
        except ValueError:
            print("Enter a valid pin")

    cursor = conn.execute("SELECT CARD_NUMBER,PIN from Account_Details;")

    for row in cursor:
        if (row[0]==card_number):
            if(user_pin==row[1]):
                return True
            else:
                return False

    conn.close()    

def check_atm_balance(withdraw_amount):
    if(withdraw_amount<atm_balance):
        return True
    else:
        return False

def update_atm_balance(withdraw_amount):
    global atm_balance
    atm_balance=atm_balance-withdraw_amount
    print("Updated atm balance")

def get_user_account_balance(card_number):
    conn = sqlite3.connect('Bank.db')

    cursor = conn.execute("SELECT CARD_NUMBER,ACCOUNT_BALANCE from Account_Details;")
    
    for row in cursor:
        if (row[0]==card_number):
                return row[1]

    conn.close()    

def update_user_balance(card_number,updated_balance_amount):

    print("Updated balance"+str(updated_balance_amount))
    conn = sqlite3.connect('Bank.db')

    cursor = conn.execute("UPDATE Account_Details SET ACCOUNT_BALANCE="+str(updated_balance_amount)+" WHERE CARD_NUMBER="+str(card_number)+";")

    conn.commit()
    conn.close()  

def withdraw_money(card_number):
    withdraw_amount=int(input("Enter the amount to be withdrawn:\n"))

    if check_atm_balance(withdraw_amount)==True:
        user_account_balance=get_user_account_balance(card_number)
        
        if user_account_balance>withdraw_amount:
            update_atm_balance(withdraw_amount)
            update_user_balance(card_number,user_account_balance-withdraw_amount)
            print("Please collect the cash")
        else:
            print("Insufficient funds in your account")
    else:
        print("Insufficient ATM balance!")

def block_card(card_number):

    conn = sqlite3.connect('Bank.db')

    cursor = conn.execute("UPDATE Account_Details SET CARD_STATUS='BLOCKED' WHERE CARD_NUMBER="+str(card_number)+";")

    conn.commit()
    conn.close() 

def main():

    pin_enter_attempts=0

    while (1):
        print("\nWelcome to ATM")

        while (1):
            try:
                card_number=int(input("Please enter your card number : \n"))
                break
            except ValueError:
                print("\nPlease enter a valid card number!")
        
        try:
            conn = sqlite3.connect('file:Bank.db?mode=rw', uri=True)
            conn.close()

        except sqlite3.OperationalError:
            print("Server Busy, please try again later.")
            continue
            
        card_validation_result=get_card_status(card_number)

        if card_validation_result== "ACTIVE":
            pin_verification_result=verify_pin(card_number)

            if pin_verification_result==True :

                withdraw_money(card_number)

            else :
                print('Incorrect pin. Try again')
                pin_enter_attempts=pin_enter_attempts+1

                if pin_enter_attempts>=3:
                    print('Card blocked')
                    block_card(card_number)
                    break
                continue
        else:
            print('Your card is '+card_validation_result+". Please contact your branch.")

main()
